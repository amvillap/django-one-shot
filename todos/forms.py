from todos.models import TodoList, TodoItem
from django.forms import ModelForm


class CreateListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class EditListNameForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class CreateNewTaskForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "list",
            "is_completed",
        ]


class UpdateTaskForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "list",
            "is_completed",
        ]
