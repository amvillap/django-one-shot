from todos.views import (
    todo_list_list,
    todo_list_details,
    create_list,
    edit_list_name,
    delete_list,
    create_new_task,
    update_task,
)
from django.urls import path

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_details, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_list_name, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_new_task, name="todo_item_create"),
    path("items/<int:id>/edit/", update_task, name="todo_item_update"),
]
