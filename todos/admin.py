from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodoList(admin.ModelAdmin):
    list_display = [
        "name",
        "created_on",
    ]


@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display: [
        "task",
        "due_date",
        "is_completed",
    ]


# Register your models here.
