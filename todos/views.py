from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import (
    CreateListForm,
    EditListNameForm,
    CreateNewTaskForm,
    UpdateTaskForm,
)


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_details(request, id):
    details = TodoList.objects.get(id=id)
    context = {"details": details}
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = CreateListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = CreateListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def edit_list_name(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = EditListNameForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = EditListNameForm(instance=todo_list)
        context = {
            "edit_list_name": form,
        }
        return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_new_task(request):
    if request.method == "POST":
        form = CreateNewTaskForm(request.POST)
        if form.is_valid():
            todo_list_details = form.save()
            return redirect("todo_list_detail", id=todo_list_details.list.id)
    else:
        form = CreateNewTaskForm()
        context = {"create_new_task_form": form}
        return render(request, "todos/create_new_task.html", context)


def update_task(request, id):
    task = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = UpdateTaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        form = UpdateTaskForm(instance=task)
        context = {
            "update_task_form": form,
        }
        return render(request, "todos/update_task.html", context)
